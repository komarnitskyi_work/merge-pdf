# Outvio NodeJS Challenge. First Task

## Intro
This solution *is not production-ready*. There was no requirements about receiving `String[]` of `base64(Buffer)` and how to return the ressulting page.

No unit tests included.

No validation by pdf's pages count. (All the pdfs will be merged into the single one and and rendered in `2x2` layout).

This solution based on LaTeX based lib [`pdfjam`](https://warwick.ac.uk/fac/sci/statistics/staff/academic-research/firth/software/pdfjam/).

## Requirements
- Docker

## How to
- `docker-compose build`
- `docker-compose up`
