const pdfjam = require('pdfjam');
const path = require('path');
const os = require('os');
const fs = require('fs');

const writePdfFromBase64 = (base64, filename) => {
	const file = new Buffer(base64, 'base64');
	fs.writeFileSync(filename, file);
	return filename;
}

const inputFiles = JSON.parse(fs.readFileSync('./input.json').toString());

async function main(files, output) {

	const tempFolder = fs.mkdtempSync(path.join(os.tmpdir(), 'pdf-merge-'));
	const tempFiles = files.map((file, i) => writePdfFromBase64(file, `${tempFolder}/${i}.pdf`));

	try {
		await pdfjam(tempFiles, {
			nup: '2x2',
			orientation: 'landscape',
			outfile: output,
			papersize: '{21cm,29.7cm}',
		});
	} catch (error) {
		throw new Error(error);

	}
}

main(inputFiles, './result.pdf')
	.then(() => {
		console.log('File was saved in ./result.pdf');
	})
	.catch(console.log)
